package com.google.formation.service;

import com.google.formation.bean.Book;

import java.util.Date;
import java.util.List;

public interface BookServiceApi {
    Integer count();

    Book findBookById(Integer id);

    List<Book> findBooks();

    Integer addBook(Book book);

    Integer updateBook(Integer id, Book book);

    void deleteBook(Integer id);
}
