package com.google.formation.controller;

import com.google.formation.bean.Book;
import com.google.formation.service.BookServiceApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
public class BookController {
    @Autowired
    private BookServiceApi bookServiceApi;

    @GetMapping(path = "/book-count")
    public ResponseEntity<Integer> bookCount() {
        return ResponseEntity.ok(bookServiceApi.count());
    }

    @GetMapping(path = "/book/{id}")
    public ResponseEntity<Book> findBookById(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(bookServiceApi.findBookById(id));
    }

    @GetMapping(path = "/books")
    public ResponseEntity<List<Book>> findBooks() {
        return ResponseEntity.ok(bookServiceApi.findBooks());
    }

    @PostMapping(path = "/add-book")
    public ResponseEntity<Integer> addBook(@RequestBody Book book) {
        return ResponseEntity.ok(bookServiceApi.addBook(book));
    }

    @PutMapping(path = "/update-book")
    public ResponseEntity<Integer> updateBook(@RequestParam("id") Integer id, @RequestBody Book book) {
        return ResponseEntity.ok(bookServiceApi.updateBook(id, book));
    }

    @DeleteMapping(path = "/delete-book/{id}")
    public ResponseEntity<Void> deleteBook(@PathVariable("id") Integer id) {
        bookServiceApi.deleteBook(id);
        return ResponseEntity.ok().build();
    }
}
