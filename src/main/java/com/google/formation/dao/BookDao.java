package com.google.formation.dao;

import com.google.formation.bean.Book;

import java.util.List;

public interface BookDao {
    Integer count();

    Book findBookById(Integer id);

    List<Book> findBooks();

    Integer addBook(Book book);

    Integer updateBook(Integer id, Book book);

    void deleteBook(Integer id);
}
