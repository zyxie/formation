package com.google.formation.dao;

import com.google.formation.bean.Book;
import com.google.formation.mapper.BookRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
@Transactional
public class BookDaoImpl implements BookDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Integer count() {
        return jdbcTemplate
                .queryForObject("select count(*) from books", Integer.class);
    }

    @Override
    public Book findBookById(Integer id) {
        return jdbcTemplate.queryForObject("select * from books where book_id = ?", new Object[] { id }, new BookRowMapper());
    }

    @Override
    public List<Book> findBooks() {
        return jdbcTemplate.query("select * from books order by book_id", new Object[] {}, new BookRowMapper());
    }

    public Integer addBook(Book book) {
        return jdbcTemplate.update("insert into books (title, author, edition_date, price) values (?, ?, ?, ?)",
                book.getTitle(), book.getAuthor(), book.getEditionDate(), book.getPrice());
    }

    @Override
    public Integer updateBook(Integer id, Book book) {
        return jdbcTemplate.update("update books set price = ? where book_id = ?", book.getPrice(), id);
    }

    @Override
    public void deleteBook(Integer id) {
        jdbcTemplate.update("delete from books where book_id = ?", id);
    }

}
